<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require('connect.php');
if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];

    $query = "INSERT INTO users (username, email, password) VALUES ('$username', '$email', '$password')";
    $result = mysqli_query($connection, $query);


    if ($result) {
        $smsg = header("location: main.php");
    } else {
        $fmsg = 'Пользователь с таким именем уже существует!';
    }
}

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="index.css">
    <title>Registration of a new employers</title>
</head>
<body>
<?php if(isset($smsg)) { ?> <div class="success" role="alert"><?php echo $smsg; ?> </div><?php }?>
<?php if(isset($fmsg)) { ?> <div class="error" role="alert"><?php echo $fmsg; ?> </div><?php }?>
    <form class="register-position" method="post">
        <h1>Регистрация</h1>
        <div class="input-form">
            <input  name="username" type="text" placeholder="Имя" required>
        </div>
        <div class="input-form">
            <input name ="email" type="email" placeholder="Почта" required>
        </div>
        <div class="input-form">
            <input name = "password" type="password" placeholder="Пароль" required>
        </div>
        <div class="input-form">
            <input type="submit" value="Регистрация">
        </div>
    </form>
</body>
</html>